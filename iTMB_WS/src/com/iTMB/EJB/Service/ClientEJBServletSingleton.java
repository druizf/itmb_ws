package com.iTMB.EJB.Service;

import java.util.Properties;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.iTMB.bean.iTMBInterfaceService;


public class ClientEJBServletSingleton {
	
	private static iTMBInterfaceService oUsuariBean = null;
	
	public static iTMBInterfaceService getInstance(){
		if (oUsuariBean == null) {
			Properties prop=new Properties();
			prop.put(Context.INITIAL_CONTEXT_FACTORY, "org.apache.openejb.client.RemoteInitialContextFactory");
			prop.put("java.naming.provider.url", "ejbd://localhost:4201");
			
			try {
				Context context = new InitialContext(prop);
				oUsuariBean = (iTMBInterfaceService) context.lookup("iTMBServiceRemote");
			} catch (NamingException e) {
				e.printStackTrace();
			}	
		}
		
		return oUsuariBean;
	}


}
