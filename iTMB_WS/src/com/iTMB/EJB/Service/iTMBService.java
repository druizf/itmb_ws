package com.iTMB.EJB.Service;

import com.iTMB.EJB.Service.ClientEJBServletSingleton;
import com.iTMB.bean.DadesusrBean;
import com.iTMB.bean.ErrorBean;
import com.iTMB.bean.ListRelUsrT11Bean;
import com.iTMB.bean.ListT11UsrInfoBean;
import com.iTMB.bean.TipTransBean;
import com.iTMB.bean.ZonaBean;
import com.iTMB.bean.iTMBInterfaceService;

public class iTMBService implements iTMBInterfaceService{
	
	
	public ErrorBean validar_Usuari(String psAlias, String psPasswd){
		return ClientEJBServletSingleton.getInstance().validar_Usuari(psAlias, psPasswd);
	}
	
	public ErrorBean crear_Usuari(String psAlias, String psPasswd, String psCorreu, String psNom, String psCognoms, int piDatanai, String psSexe, String psTelf){
		return ClientEJBServletSingleton.getInstance().crear_Usuari(psAlias, psPasswd, psCorreu, psNom, psCognoms, piDatanai, psSexe, psTelf);
	}
	
	public ErrorBean modificar_Usuari(String psAlias, String psPasswd, String psCorreu, String psNom, String psCognoms, int piDatanai, String psSexe, String psTelf){
		return ClientEJBServletSingleton.getInstance().modificar_Usuari(psAlias, psPasswd, psCorreu, psNom, psCognoms, piDatanai, psSexe, psTelf);
	}
	
	public DadesusrBean obtenir_dades_Usuari(String psAlias){
		return ClientEJBServletSingleton.getInstance().obtenir_dades_Usuari(psAlias);
	}
	
	public ErrorBean esborrar_Usuari(String psAlias){
		return ClientEJBServletSingleton.getInstance().esborrar_Usuari(psAlias);
	}
	
	public ZonaBean obtenir_Zones() {
		return ClientEJBServletSingleton.getInstance().obtenir_Zones();
	}
	
	public ErrorBean es_Zona(String psZona) {
		return ClientEJBServletSingleton.getInstance().es_Zona(psZona);
	}
	
	public TipTransBean obtenir_Tip_Transports(){
		return ClientEJBServletSingleton.getInstance().obtenir_Tip_Transports();
	}
	
	public ErrorBean es_Tip_Transports(String psTrans) {
		return ClientEJBServletSingleton.getInstance().es_Tip_Transports(psTrans);
	}
	
	public ErrorBean es_trans_compatible(String psTrans1, String psTrans2){
		return ClientEJBServletSingleton.getInstance().es_trans_compatible(psTrans1, psTrans2);
	}

	public ErrorBean oferir_T11(String psAlias, 
			String psTrans, String psZona, String psDataFnl, String ptHoraFnl, double pdLat, double pdLng, String psMiss) {
		return ClientEJBServletSingleton.getInstance().oferir_T11(psAlias, psTrans, psZona, psDataFnl, ptHoraFnl, pdLat, pdLng, psMiss);
	}

	public ListT11UsrInfoBean disponibles_T11(String psTrans, double pdLat, double pdLng) {
		return ClientEJBServletSingleton.getInstance().disponibles_T11(psTrans, pdLat, pdLng);
	}

	public ErrorBean reservar_T11(int piIDRel, String psAlias, String psTrans, double pdLat, double pdLng) {
		return ClientEJBServletSingleton.getInstance().reservar_T11(piIDRel, psAlias, psTrans, pdLat, pdLng);
	}

	public ErrorBean cancelar_T11(int piIDRel, String psAlias) {
		return ClientEJBServletSingleton.getInstance().cancelar_T11(piIDRel, psAlias);
	}

	public ErrorBean confirmar_T11(int piIDRel, String psAlias) {
		return ClientEJBServletSingleton.getInstance().confirmar_T11(piIDRel, psAlias);
	}

	public ListRelUsrT11Bean Llistat_T11_Emissor(String psAlias) {
		return ClientEJBServletSingleton.getInstance().Llistat_T11_Emissor(psAlias);
	}

	public ListRelUsrT11Bean Llistat_T11_Receptor(String psAlias) {
		return ClientEJBServletSingleton.getInstance().Llistat_T11_Receptor(psAlias);
	}	
}
