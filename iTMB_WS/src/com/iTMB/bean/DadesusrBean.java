package com.iTMB.bean;

import java.io.Serializable;

public class DadesusrBean implements Serializable {
	
	private static final long serialVersionUID = -4899257565992711084L;
	
	private String alias;

	private String passwd;
	
	private String correu;

	private String cognoms;

	private int datanai;

	private String nom;

	private String sexe;

	private String telf;

	ErrorBean oErrorBean;
	
	public DadesusrBean() {
		super();
		
		this.alias = "";
		this.passwd = "";
		this.correu = "";
		this.cognoms = "";
		this.datanai = 0;
		this.nom = "";
		this.sexe = "";
		this.telf = "";
		this.oErrorBean = new ErrorBean();
	}
	
	public DadesusrBean(String psAlias, String psPasswd, String psCorreu, String psCognoms, int piDatanai, String psNom, String psSexe, String psTelf, ErrorBean poErrorBean) {
		super();
		
		this.alias = psAlias;
		this.passwd = psPasswd;
		this.correu = psCorreu;
		this.cognoms = psCognoms;
		this.datanai = piDatanai;
		this.nom = psNom;
		this.sexe = psSexe;
		this.telf = psTelf;
		this.oErrorBean = poErrorBean;
	}
	
	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public String getCorreu() {
		return correu;
	}

	public void setCorreu(String correu) {
		this.correu = correu;
	}

	public String getCognoms() {
		return cognoms;
	}

	public void setCognoms(String cognoms) {
		this.cognoms = cognoms;
	}

	public int getDatanai() {
		return datanai;
	}

	public void setDatanai(int datanai) {
		this.datanai = datanai;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getSexe() {
		return sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	public String getTelf() {
		return telf;
	}

	public void setTelf(String telf) {
		this.telf = telf;
	}

	public ErrorBean getErrorBean() {
		return this.oErrorBean;
	}
	
	public void setErrorBean(ErrorBean oErrorBean) {
		this.oErrorBean = oErrorBean;
	}
	
	public String toString(){
		String sRetValue = null;
		
		sRetValue = "Dades usuari: "                  + "\n"   +
					"Alias: "          + this.alias   + "\n"   +
					"PassWd: "         + this.passwd  + "\n"   +
					"Correu: "         + this.correu  + "\n"   +
					"Nom: "            + this.nom     + "\n"   +
					"Cognoms: "        + this.cognoms + "\n"   +
					"Data naixament: " + this.datanai + "\n"   +
					"Sexe: "           + this.sexe    + "\n"   +
					"Telefon: "        + this.telf    + "\n\n" +
					"Error: "                                                   + "\n"   +
					"Codi: "           + this.oErrorBean.getCodi()              + "\n"   + 
					"Descripció: "     + this.oErrorBean.getDescripcio() ;
				
		return sRetValue;
	}
	

}
