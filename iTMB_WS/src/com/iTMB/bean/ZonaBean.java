package com.iTMB.bean;

import java.io.Serializable;

public class ZonaBean implements Serializable {
	
	private static final long serialVersionUID = 1L;

	String[] zona;

	ErrorBean oErrorBean;

	public ZonaBean(int piNumZonas) {
		super();
		
		this.zona = new String[piNumZonas];
		this.oErrorBean = new ErrorBean();
	}

	public String[] getZona() {
		return this.zona;
	}

	public void setZona(int piPos, String psZona) {
		if (this.zona != null)
			this.zona[piPos] = psZona;
	}
	
	public void setZona(String[] psZona) {
		if (psZona != null) {
			this.zona = new String[psZona.length];
			for (int i = 0; (i < psZona.length - 1); i++)
				this.zona[i] = psZona[i]; 
		} 
		else this.zona = null;
	}
	
	public ErrorBean getErrorBean() {
		return this.oErrorBean;
	}
	
	public void setErrorBean(ErrorBean oErrorBean) {
		this.oErrorBean = oErrorBean;
	}

	
	public String toString(){
		String sRet = null;
		
		sRet = "Zones: \n";
		
		for (int i = 0; i < this.zona.length; i++) {
			sRet = sRet + this.zona[i] + "\n";
		}
		
		sRet = sRet + "Error:"        +                        "\n" +
			          "Codi: "        + oErrorBean.getCodi() + "\n" +
					  "Descripció: "  + oErrorBean.getDescripcio() + "\n";
	
		return sRet;
	}
}