package com.iTMB.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListT11UsrInfoBean implements Serializable {

	private static final long serialVersionUID = -188037281590194902L;
	
	ArrayList<T11UsrInfoBean> lT11UsrInfoBean;
	
	ErrorBean oErrorBean;
	
	public ListT11UsrInfoBean(){
		this.lT11UsrInfoBean = new ArrayList<T11UsrInfoBean>();
		this.oErrorBean = new ErrorBean();
	}

	public List<T11UsrInfoBean> getlT11UsrInfoBean() {
		return this.lT11UsrInfoBean;
	}

	public void setlT11UsrInfoBean(ArrayList<T11UsrInfoBean> lT11UsrInfoBean) {
		this.lT11UsrInfoBean = lT11UsrInfoBean;
	}

	public ErrorBean getoErrorBean() {
		return oErrorBean;
	}

	public void setoErrorBean(ErrorBean oErrorBean) {
		this.oErrorBean = oErrorBean;
	}
	
}
