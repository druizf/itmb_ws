package com.iTMB.bean;

import java.io.Serializable;
import java.util.ArrayList;

public class ListRelUsrT11Bean implements Serializable {

	private static final long serialVersionUID = -188037281590194902L;
	
	ArrayList<RelusrT11Bean> lRelusrT11Bean;
	
	ErrorBean oErrorBean;
	
	public ListRelUsrT11Bean(){
		this.lRelusrT11Bean = new ArrayList<RelusrT11Bean>();
		this.oErrorBean = new ErrorBean();
	}

	public ArrayList<RelusrT11Bean> getlRelusrT11Bean() {
		return lRelusrT11Bean;
	}

	public void setlRelusrT11Bean(ArrayList<RelusrT11Bean> lRelusrT11Bean) {
		this.lRelusrT11Bean = lRelusrT11Bean;
	}

	public ErrorBean getoErrorBean() {
		return oErrorBean;
	}

	public void setoErrorBean(ErrorBean oErrorBean) {
		this.oErrorBean = oErrorBean;
	}
	
}
