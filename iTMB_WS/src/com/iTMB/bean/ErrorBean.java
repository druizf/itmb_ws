package com.iTMB.bean;

import java.io.Serializable;

public class ErrorBean implements Serializable {

	private static final long serialVersionUID = -144033007747540694L;
	
	private String sCodi;
	private String sDesc;
	
	public ErrorBean(){
		super();
		
		this.sCodi = "";
		this.sDesc = "";
	}
	
	public ErrorBean(String psCodi, String psDesc){
		super();
		
		this.sCodi = psCodi;
		this.sDesc = psDesc;
	}
	
	public ErrorBean(ErrorBean poErrorBean){
		super();
		
		if (poErrorBean == null) {
			this.sCodi = "";
			this.sDesc = "";
		} else {
			this.sCodi = poErrorBean.sCodi;
			this.sDesc = poErrorBean.sDesc;
		}
	}
	
	public void setCodi(String psCodi){
		this.sCodi = psCodi;
	}
	
	public String getCodi(){
		return this.sCodi;
	}
	
	public void setDescripcio(String psDesc){
		this.sDesc = psDesc;
	}
	
	public String getDescripcio(){
		return this.sDesc;
	}
	
	public String toString(){
		String sRetValue = null;
		
		sRetValue = "Codi: "       + this.sCodi + "\n" +
					"Descripció: " + this.sDesc;
		
		return sRetValue;
	}
	
}
