package com.iTMB.bean;

import java.io.Serializable;

public class T11UsrInfoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String alias;

	private int numofr;

	private int numrsvAcp;

	private int numrsvCan;

	private int numtrt;
	
	private int idRelT11;

	public T11UsrInfoBean(){
		this.alias = "";
		this.numofr = 0;
		this.numrsvAcp = 0;
		this.numrsvCan = 0;
		this.numtrt = 0;
	}
	
	public int getIdRelT11() {
		return idRelT11;
	}

	public void setIdRelT11(int idRelT11) {
		this.idRelT11 = idRelT11;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public int getNumofr() {
		return numofr;
	}

	public void setNumofr(int numofr) {
		this.numofr = numofr;
	}

	public int getNumrsvAcp() {
		return numrsvAcp;
	}

	public void setNumrsvAcp(int numrsvAcp) {
		this.numrsvAcp = numrsvAcp;
	}

	public int getNumrsvCan() {
		return numrsvCan;
	}

	public void setNumrsvCan(int numrsvCan) {
		this.numrsvCan = numrsvCan;
	}

	public int getNumtrt() {
		return numtrt;
	}

	public void setNumtrt(int numtrt) {
		this.numtrt = numtrt;
	}
	
}
